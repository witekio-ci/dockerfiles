#!/bin/sh

# Fix default uid/gid to 1000
uid=${uid:-1000}
gid=${gid:-1000}
user=__USERNAME__
group=$user
home=/home/${user}

groupadd -g ${gid} ${group}
useradd -c "Jenkins user" -d /home/${user} -u ${uid} -g ${gid} -M ${user}

chown -R ${user}:${group} ${home}/.ssh
chown ${user}:${group} ${home}/.gitconfig
chown ${user}:${group} ${home}

# Switch to the user we just added and run the given command or /bin/sh
exec /bin/su ${user} -c "${@:-/bin/sh}"
